<?php
session_start();

?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<title>Конвертер валют</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<header>
	<h1>Конвертер валют (made by Dmitry Alekseiev)</h1>
</header>

<div class="container">
	<?php
	if($_SESSION['logged_user']) {
	?>
        Добро пожаловать <?php echo $_SESSION['logged_user']['login']; ?>. Нажми чтобы <a href="logout.php" tite="Logout">Выйти</a>
    <?php
    }
    ?>
	<form class="form-control" action="" id="convert_form" method="GET">
		<label for="">
			<input type="number" name="amount" id="amount_id" placeholder="Введите сумму">
		</label>
		<label for="">
			<select name="from" id="from_id">
				<option value="UAH">Гривна</option>
				<option value="RUR">Рубль</option>
				<option value="USD">Доллар</option>
				<option value="EUR">Евро</option>
			</select>
		</label>

		<label for="">
			<select name="to" id="to_id">
				<option value="UAH">Гривна</option>
				<option value="RUR">Рубль</option>
				<option value="USD">Доллар</option>
				<option value="EUR">Евро</option>
			</select>
		</label>
		<div class="convert-result" id="result_id">
			<p>00,000</p>
		</div>
		<input type="submit" name="submit" value="Конвертировать">
	</form>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="js/ajax.js"></script>
</body>
</html>

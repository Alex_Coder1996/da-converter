<?php
require "config.php";

$login = filter_var(trim($_POST['login']), FILTER_SANITIZE_STRING);
$password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);

$password = password_hash($password, PASSWORD_DEFAULT);

// connecting to db
try {
	$pdo = new PDO( "mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass );
} catch ( PDOException $e ) {
	echo "Connection failed: " . $e->getMessage();
}

$sql = $pdo->prepare("SELECT * FROM `users` WHERE `login` = '$login'");
$sql->execute();
$user1 = $sql->fetchAll();

if (!empty($user1) ){
	echo "Данный логин уже используется!";
	exit();
}

// registration
$sql   = ( "INSERT INTO `users` (`login`, `password`) VALUES(?,?)" );
$query = $pdo->prepare( $sql );
$query->execute( [ $login, $password ] );

header('Location: index.php');
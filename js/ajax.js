$(document).ready(function() {
    $("#convert_form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: 'func.php',
            data: $(this).serialize(),
            success: function(response) {
                $('.convert-result').html(response);
            },
            error: function(xhr, str) {
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });
    });
});
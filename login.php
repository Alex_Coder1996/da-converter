<?php
session_start();
require "config.php";

$login = filter_var(trim($_POST['login']), FILTER_SANITIZE_STRING);
$password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);

// connecting to db
try {
	$pdo = new PDO( "mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass );
} catch ( PDOException $e ) {
	echo "Connection failed: " . $e->getMessage();
}

$sql = $pdo->prepare( "SELECT * FROM `users` WHERE `login` = '$login'" );
$sql->execute();
$user = $sql->fetchAll();

if( count( $user ) == 0 ) {
	echo "Такой пользователь не найден.";
	exit();
} elseif( count( $user ) == 1 ) {
	if( password_verify($password, $user[0]['password'] ) ) {

		$_SESSION['logged_user'] = $user;

		header('Location: converter.php');

	} else {

		echo 'Пароль неверно введен!';

	}
}


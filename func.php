<?php

require "config.php";

$courses = json_decode( file_get_contents( $link ), true );
//print_r($courses);
$rur_to_hryvna = floatval( $courses[2]['buy'] );
$eur_to_hryvna = floatval( $courses[1]['buy'] );
$usd_to_hryvna = floatval( $courses[0]['buy'] );

$usd_to_rur = $usd_to_hryvna / $rur_to_hryvna;
$eur_to_rur = $eur_to_hryvna / $rur_to_hryvna;

$eur_to_usd = $eur_to_hryvna / $usd_to_hryvna;


function convertCurrency($value, $coef_currency) {

		return $value * $coef_currency;

}

if ( isset( $_GET['amount'] ) && isset( $_GET['from'] ) && isset( $_GET['to'] ) ) {

	$amount = $_GET['amount'];
	$from   = $_GET['from'];
	$to     = $_GET['to'];

	if ( $from == 'RUR' && $to == 'UAH') {
		$coef = $rur_to_hryvna;
	} elseif ( $from== 'EUR' && $to == 'UAH' ) {
		$coef = $eur_to_hryvna;
	} elseif ( $from == 'USD' && $to == 'UAH' ) {
		$coef = $usd_to_hryvna;
	} elseif ( $from == 'RUR' && $to == 'USD' ) {
		$coef = 1 / $usd_to_rur;
	} elseif ( $from == 'UAH' && $to == 'USD' ) {
		$coef = 1 / $usd_to_hryvna;
	} elseif ( $from == 'EUR' && $to == 'USD' ) {
		$coef = $eur_to_usd;
	} elseif ( $from == 'RUR' && $to == 'EUR' ) {
		$coef = 1 / $eur_to_rur;
	} elseif ( $from == 'UAH' && $to == 'EUR' ) {
		$coef = 1 / $eur_to_hryvna;
	} elseif ( $from == 'USD' && $to == 'EUR' ) {
		$coef = 1 / $eur_to_usd;
	} elseif ( $from == 'UAH' && $to == 'RUR' ) {
		$coef = 1 / $rur_to_hryvna;
	} elseif ( $from == 'EUR' && $to == 'RUR' ) {
		$coef = $eur_to_rur;
	} elseif ( $from == 'USD' && $to == 'RUR' ) {
		$coef = $usd_to_rur;
	}

echo convertCurrency( $amount, $coef );

}


